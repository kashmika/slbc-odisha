import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginFormComponent } from './login-form/login-form.component';
import { Page1Component } from 'src/app/page1/page1.component';
import { Page2Component } from 'src/app/page2/page2.component';


const routes: Routes = [
     { path : '', component : LoginFormComponent},
    { path : 'page1', component :  Page1Component},
    { path : 'page2', component :  Page2Component}
 ];
 
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

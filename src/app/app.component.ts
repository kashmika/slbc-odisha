import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
// import { HttpErrorResponse } from '@angular/common/http';
import { LoginFormComponent } from './login-form/login-form.component';
import {UserService } from './user.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Slbc-Odisha';
}

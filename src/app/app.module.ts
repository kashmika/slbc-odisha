import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
 import { HttpModule } from '@angular/http';
 import { RouterModule } from '@angular/router';
 import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { User } from './user.class';
import { FormGroup,FormBuilder,Validators,FormControl  } from '@angular/forms';

import { AppComponent } from './app.component';
import { UserService } from './user.service'; 
import { LoginFormComponent } from './login-form/login-form.component';
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    Page1Component,
    Page2Component,
   
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
 HttpClientModule,
 
  ],
  providers: [FormBuilder,UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }

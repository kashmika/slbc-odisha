import { Component, OnInit } from '@angular/core';
import { FormsModule }   from '@angular/forms'; 
import { IUser } from '../user';
import{ Observable } from 'rxjs';

import { UserService } from '../user.service'; 
import { Http,Response } from '@angular/http';
//import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import {  FormGroup,FormBuilder,Validators,FormControl  } from '@angular/forms';

 

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
  providers: [ UserService ]
})
export class LoginFormComponent implements OnInit {
  public  processes=[];


  constructor (private newService : UserService,
    private formBuilder: FormBuilder,
  private http:Http, ) {}

  ngOnInit () {
this.newService.getUser()
.subscribe(data=>{
  this.processes=data
  console.log( this.processes)
})

}
}

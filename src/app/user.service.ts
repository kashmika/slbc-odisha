import { Injectable } from '@angular/core';
import { IUser } from './user'; 
import { Jsonp, URLSearchParams } from '@angular/http';  
//import { Http,Response } from '@angular/http';
import{HttpClient} from '@angular/common/http';
import { Observable, Subject, pipe } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable()
export class UserService {
  private _url:string="./assets/acc.json";

  constructor(  
    private http:HttpClient){
  
  }
  // fetchData(){
  //  return this.http.get('./assets/acc.json').pipe(
  //  map((res:Response)=>res.json()))
  //   // .subscribe((data)=>console.log(data))
  // }

  getUser():Observable<IUser[]>{
    return this.http.get<IUser[]>(this._url);
  } 
  

}

